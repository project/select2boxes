<?php

namespace Drupal\Tests\select2_bef\FunctionalJavascript;

use Drupal\Tests\select2boxes\FunctionalJavascript\Select2BoxesTestsBase;

/**
 * Class for testing Select2Bef.
 *
 * @group Select2Bef
 */
class Select2BefTest extends Select2BoxesTestsBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'select2boxes',
    'select2boxes_test',
    'select2_bef',
    'better_exposed_filters',
    'jquery_ui',
    'jquery_ui_datepicker',
    'jquery_ui_slider',
    'jquery_ui_touch_punch',
    'node',
    'field',
    'views',
    'views_ui',
    'user',
  ];

  /**
   * Exempt from strict schema checking.
   *
   * @var bool
   *
   * @see \Drupal\Core\Config\Development\ConfigSchemaChecker
   *
   * @todo This is really evil and should be removed as soon as the fields in
   *   \Drupal\select2_bef\Plugin\better_exposed_filters\filter\Select2Boxes::defaultConfiguration
   *   have their own schema. This would however be hard to do without breaking
   *   backwards compatibility.
   *   https://www.drupal.org/project/select2boxes/issues/3217960
   */
  protected $strictConfigSchema = FALSE;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $select2befTestUser = $this->createUser([
      'create select2boxes_test_content_type content',
      'edit own select2boxes_test_content_type content',
      'administer node form display',
      'administer site configuration',
      'administer node fields',
      'administer views',
    ]);

    $this->drupalLogin($select2befTestUser);
    $this->drupalLogin($this->rootUser);
  }

  /**
   * Test filter for tag fields.
   */
  public function testFilterForTagFields() {
    $page = $this->getSession()->getPage();
    $assert_session = $this->assertSession();

    // Generate fake contents for testing.
    $this->generateDummyTerms('tags', 10);
    $this->generateDummyContent(10);

    $this->drupalGet('admin/structure/views/view/test');

    // Add filter by field "Tags" using the View's UI.
    $this->click('a[id="views-add-filter"]');
    $assert_session->assertWaitOnAjaxRequest();
    $page->fillField('override[controls][options_search]', 'tags');
    $page->checkField('name[node__field_tags.field_tags_target_id]');

    $assert_session->pageTextContains('Selected: Tags (field_tags)');
    $this->click('button.button--primary');
    $assert_session->assertWaitOnAjaxRequest();

    // Set dropdown as a filter widget.
    $page->fillField('options[type]', 'select');
    $this->click('button.button--primary');
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->pageTextContains('Configure filter criterion 2 of 2: Content: Tags (field_tags)');

    // Make filter exposed.
    $page->checkField('options[expose_button][checkbox][checkbox]');
    $assert_session->assertWaitOnAjaxRequest();
    $this->click('button.button--primary');
    $assert_session->waitForElementRemoved('css', '.views-ui-dialog');

    // Save the view.
    $page->pressButton('Save');
    $assert_session->pageTextContains('The view Test has been saved.');
    $assert_session->pageTextContains('Content: Tags (exposed)');

    $this->drupalGet('admin/structure/views/nojs/display/test/page_1/exposed_form');
    $edit = [
      'exposed_form[type]' => 'bef',
    ];
    $this->submitForm($edit, 'Apply');

    $this->drupalGet('admin/structure/views/nojs/display/test/page_1/exposed_form_options');
    $this->click('summary[role="button"]');
    $assert_session->assertWaitOnAjaxRequest();
    $page->selectFieldOption('exposed_form_options[bef][filter][field_tags_target_id][configuration][plugin_id]', 'select2boxes');
    $page->pressButton('Apply');
    $page->pressButton('Save');

    $assert_session->pageTextContains('The view Test has been saved.');

    $this->drupalGet('test');
    $select = $assert_session->elementExists('css', '#edit-field-tags-target-id');

    // Check if all required html attributes are existing on the entity
    // reference field.
    $assert_session->elementAttributeExists('css', '#edit-field-tags-target-id', 'data-jquery-once-autocomplete');
    $assert_session->elementAttributeExists('css', '#edit-field-tags-target-id', 'data-select2-autocomplete-list-widget');
    $this->assertTrue($select->hasClass('select2-widget'));

    // Update widget to use multiple Select2 widget.
    $this->drupalGet('admin/structure/views/view/test');
    $this->clickLink('Content: Tags (exposed)');
    $assert_session->assertWaitOnAjaxRequest();
    $page->checkField('options[expose][multiple]');
    $this->click('button.button--primary');
    $assert_session->waitForElementRemoved('css', '.views-ui-dialog');

    $this->drupalGet('admin/structure/views/nojs/display/test/page_1/exposed_form_options');
    $page->selectFieldOption('exposed_form_options[bef][filter][field_tags_target_id][configuration][plugin_id]', 'select2boxes');
    $page->pressButton('Apply');
    $page->pressButton('Save');

    $assert_session->pageTextContains('The view Test has been saved.');

    $this->drupalGet('test');
    // Trigger the opening dropdown via click on the search input field.
    $this->click('input[class="select2-search__field"]');
    $assert_session->assertWaitOnAjaxRequest();

    $select = $assert_session->elementExists('css', '#edit-field-tags-target-id');

    // Check if all required html attributes are existing for the entity
    // reference field.
    $assert_session->elementAttributeExists('css', '#edit-field-tags-target-id', 'data-jquery-once-autocomplete');
    $assert_session->elementAttributeExists('css', '#edit-field-tags-target-id', 'data-select2-autocomplete-list-widget');
    $assert_session->elementAttributeExists('css', '#edit-field-tags-target-id', 'data-select2-multiple');
    $this->assertTrue($select->hasClass('select2-widget'));
  }

  /**
   * Test list exposed filters.
   */
  public function testListExposedFilters() {
    $page = $this->getSession()->getPage();
    $assert_session = $this->assertSession();

    // Generate fake contents for testing.
    $this->generateDummyTerms('tags', 10);
    $this->generateDummyContent(10);

    $this->drupalGet('admin/structure/views/view/test');

    // Add filter by test list field using the View's UI.
    $this->click('a[id="views-add-filter"]');
    $assert_session->assertWaitOnAjaxRequest();
    $page->fillField('override[controls][options_search]', 'Test');
    $page->checkField('name[node__field_test_list.field_test_list_value]');
    $this->click('button.button--primary');
    $assert_session->waitForElementRemoved('css', '.views-ui-dialog');

    // Make filter exposed.
    $page->checkField('options[expose_button][checkbox][checkbox]');
    $assert_session->assertWaitOnAjaxRequest();
    $this->click('button.button--primary');
    $assert_session->waitForElementRemoved('css', '.views-ui-dialog');

    $this->drupalGet('admin/structure/views/nojs/display/test/default/exposed_form');
    $edit = [
      'exposed_form[type]' => 'bef',
    ];
    $this->submitForm($edit, 'Apply');

    $this->drupalGet('admin/structure/views/nojs/display/test/default/exposed_form_options');
    $this->click('summary[role="button"]');
    $page->selectFieldOption('exposed_form_options[bef][filter][field_test_list_value][configuration][plugin_id]', 'select2boxes');
    $page->pressButton('Apply');
    $page->pressButton('Save');

    $assert_session->pageTextContains('The view Test has been saved.');

    $this->drupalGet('test');
    $select = $assert_session->elementExists('css', '#edit-field-test-list-value');

    // Check if all required html attributes are existing for the entity
    // reference field.
    $assert_session->elementAttributeExists('css', '#edit-field-test-list-value', 'data-jquery-once-autocomplete');
    $assert_session->elementAttributeExists('css', '#edit-field-test-list-value', 'data-select2-autocomplete-list-widget');
    $this->assertTrue($select->hasClass('select2-widget'));
  }

}
