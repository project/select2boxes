<?php

namespace Drupal\Tests\select2boxes\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;

/**
 * Class Select2BoxesTestsBase.
 *
 * Base class for the Select2Boxes tests.
 *
 * @group Select2Boxes
 */
abstract class Select2BoxesTestsBase extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules that needs to be enabled.
   *
   * @var array
   */
  protected static $modules = [
    'field',
    'field_ui',
    'node',
    'options',
    'select2boxes',
    'select2boxes_test',
    'taxonomy',
    'views',
  ];

  /**
   * User with sufficient permissions to run all the tests.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $user;

  /**
   * Plugin IDs list of the field widgets.
   *
   * @var array
   */
  protected static $pluginIds = [
    'select2boxes_autocomplete_list',
    'select2boxes_autocomplete_single',
    'select2boxes_autocomplete_multi',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->user = $this->createUser([
      'create select2boxes_test_content_type content',
      'edit own select2boxes_test_content_type content',
      'administer node form display',
      'administer site configuration',
      'administer node fields',
    ]);
  }

  /**
   * Generates a specified number of terms within a specific vocabulary.
   *
   * @param string $vid
   *   Taxonomy vocabulary ID.
   * @param int $count
   *   Number of terms that needs to be generated.
   *
   * @return \Drupal\taxonomy\Entity\Term[]
   *   Generated terms objects.
   */
  protected function generateDummyTerms($vid, $count) {
    $terms = [];
    for ($i = 0; $i < $count; $i++) {
      $terms[$i] = Term::create([
        'vid' => $vid,
        'name' => $this->randomString(4),
      ]);
      $terms[$i]->save();
    }

    return $terms;
  }

  /**
   * Generates a specified number of nodes of select2boxes_test_content_type.
   *
   * @param int $count
   *   Number of nodes that needs to be generated.
   *
   * @return \Drupal\node\Entity\Node[]
   *   Generated nodes objects.
   */
  protected function generateDummyContent($count) {
    $nodes = [];
    for ($i = 0; $i <= $count; $i++) {
      $nodes[$i] = Node::create([
        'type' => 'select2boxes_test_content_type',
        'title' => $this->randomString(4),
      ]);
      $nodes[$i]->save();
    }

    return $nodes;
  }

}
